from django.shortcuts import render, redirect
from tasks.forms import CreateTaskProjectForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskProjectForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    irrelevant = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": irrelevant,
    }
    return render(request, "tasks/mine.html", context)
